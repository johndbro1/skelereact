# skelereact

The skeleton of an app that supports React and React-native clients, and includes a Java Spring-Boot server to actually service the API calls and interact with the database.   

IMO, this is the best of both worlds - React is delightful as a way to create your front-end (especially with the ability to migrate between native and web with ease), but in my experience, JavaScript is a nightmare to work with for server-side data, especially in an Enterprise solution.