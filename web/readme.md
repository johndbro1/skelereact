this is the React web implementation for skelereact.   

It will basically be a login page, which you will always be forced to use until logged in,
and then a single "secret" page that you can't see until you're logged in.  

Almost all apps use this pattern, so if I can get this to work properly, it makes setting up
a new project a lot easier

