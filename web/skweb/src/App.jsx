import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route } from 'react-router-dom';
import { connect } from 'react-redux';

// import { alertActions } from './actions';
import { AuthenticatedRoute } from './routing';
import { LandingPage } from './pages/LandingPage';
import { HomePage } from './pages/HomePage';
import { LoginPage } from './pages/LoginPage';



/*
   This is the framework of the application.  It defers most of the complex and/or useful
   work to the LoginPage and HomePage sub-components, and it depends on the BrowserRouter
   component to decide which of those needs to be shown on the screen.

   As you can see, this component is connected to redux, but only enough so that it can
   show alerts and errors with a little flair.
 */
class _App extends Component {

    render() {
        const { alert } = this.props;
        return (

            <div className="container">
              <div className="jumbotron">
                      <div className="col-sm-8 col-sm-offset-2">
                          {alert.message &&
                              <div className={`alert ${alert.type}`}>{alert.message}</div>
                          }
                      </div>
              </div>
              <div>
                    <BrowserRouter>
                        <div>
                            <Route path="/" exact component={LandingPage} />
                            <AuthenticatedRoute exact path="/home" component={HomePage} />
                            <Route path="/login" component={LoginPage} />
                        </div>
                    </BrowserRouter>
              </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { alert } = state;
    return {
        alert
    };
}

const App = connect(mapStateToProps)(_App);
export default App;


/*
class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
      </div>
    );
  }
}

export default App;
*/