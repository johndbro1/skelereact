import { authConstants } from '../constants';
import { loginService } from '../services';
import { alertActions } from './';

export const authActions = {
    login,
    logout
};

function login(username, password, history) {

	// standard redux action functions return actions, but we're not returning an action,
	// we're returning a function, called dispatch.  That's because the thunk middleware 
	// is designed to detect when the result is a function, instead of an action object, and
	// intercepts it, and executes the function itself, at a time and place of its choosing.
	//
    return dispatch => {

    	// tell the react app that we're starting the request to log in.  This is a drama in four parts:

    	// 1. we dispatch the request function, which causes it to get run by redux  
    	// 2. request() returns an action, and the standard redux middleware
    	// accepts the action, and calls all of the reducers.
    	// 3. only one reducer (authentication.reducer) cares about the action type LOGIN_REQUEST, and all it
    	// does is set the state variable loggingIn to true.
    	// 4. when that state variable changes, the LoginPage gets notified because (if you look at LoginPage.jsx)
    	// LoginPage was composed with the "connect" function, which creates a more direct connection between
    	// LoginPage and the redux state.  How does it create this more direct connection?
    	//   via the mapStateToProps, which knows what the authentication sub-state looks like , and happens to
    	//   know that the authentication sub-state includes a property called loggingIn. mapStateToProps turns
    	//   that state into a property, sets that property on LoginPage, and because the property changed, 
    	//   LoginPage.render() is called again, which re-runs all the rendering logic, which includes whether
    	//   to display an animated "wait" icon
        dispatch(request({ username }));

        console.log("auth.actions.jsx - login() - about to call loginService");

        // now we call the actual service, which attempts to reach out to the destination to do work.
        // if you look inside the login.service code, it's using a Promise to manage the asynchronous logic
        loginService.login(username, password)
            .then(

            	// structurally, we're providing the "then" function of the Promise with two functions as parameters,
            	// one that accepts anything that isn't an error, which we decide to call "user" because we choose
            	// to call it that.  The second function is the error handler.

                user => { 

                	console.log("auth.action - login() - successfully logged in, user data is: "+JSON.stringify(user));

                	// ok, we got a successful user.  
                	// 0. it's subtle, but in the loginService code, it adds the user object (in string form) to 
                	//    local storage.   We'll come back to that in a moment.
                	// 1. We dispatch the success action handler, and the user object to redux, 
                	// 2. redux calls the authentication.reducer. 
                	// 3. when authentication.reducer receives a LOGIN_SUCCESS response, it updates
                	//    the authentication substate, setting loggingIn to false, and adding the
                	//    exploded user object to the authentication sub-state.
                	// 4. The change in state will redux to re-render the LoginPage, which causes the LoginPage 
                	//    to stop animating, but it doens't really matter because immediately after the dispatch, 
                	//    we call history.push("/home") 
                	// 5. calling history.push() is a top-level routing thing, and if you look at App.jsx, you'll
                	//    see that it has a special routing rule for "/home", called AuthenticatedRoute
                	// 6. AuthenticatedRoute looks at the user object in local storage.  If it finds one, it
                	//    activates the specified component, otherwise, it activates the LoginPage component.
                	//  
                    dispatch(success(user));
                    dispatch(alertActions.clear());                    
					history.push("/home");
                },
                error => {


                	console.log("auth.actions login() error: "+JSON.stringify(error.message));
                	// this is the error handler, we have to pop up an error message, which is 
                	// managed by the alertActions, and we dispatch failure, which does very little
                	// except destroy all of the authentication-related sub-state .  That, in turn,
                	// forces a re-render of LoginPage, which will clean up the 

                    dispatch(failure(error.message));
                    dispatch(alertActions.error(error.message));
                }
            );
    };

    function request(user) { return { type: authConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: authConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: authConstants.LOGIN_FAILURE, error } }
}


// this is a minimalistic function that just clears out state - it doesn't even
// tell the server.  That might  change in a production app.
//
function logout() {
    loginService.logout();
    return { type: authConstants.LOGOUT };
}

