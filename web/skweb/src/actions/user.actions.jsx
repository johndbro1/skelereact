// user actions
import { userConstants } from '../constants';
import { userService } from '../services';
import { alertActions } from './';


export const userActions = {
    getProfile,
    getData
};

/*
	Just to prove that we can get data post-login, I added this - it calls a different url post login,
	and the /api/profile service on the server requires the JWT header in order to determine who the
	user is.  So we're demonstrating post-login data fetching and authenticated user data fetching in
	one simple sevice.
*/
function getProfile()  {

	return dispatch => {

		// I left this in here so we could include animation or other UI sugar during the fetch
		// process, although in practice it's not really necessary.
        dispatch(request(""));

        userService.getProfile()
            .then(

            	// like the login service, we're calling the getProfile() function, which returns
            	// a promise.  The promise calls either the success or failure function, which we 
            	// have called profile (for success) and error.  Then we dispatch to the actions
            	// which then call the reducers, which then update the state, which
            	// causes react to re-render the components that care about that state.
            	// 
                profile => { 

                	console.log("userActions.getProfile: Successfully retrieved the profile for the user");
                    dispatch(success(profile));
                    dispatch(alertActions.clear());
                },
                error => {

                	console.log("userActions.getProfile: Problem fetching the profile for the user: "+JSON.stringify(error));

                    dispatch(failure(error));
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request(data)    { return { type: userConstants.PROFILE_REQUEST, data } }
    function success(profile) { return { type: userConstants.PROFILE_SUCCESS, profile } }
    function failure(error)   { return { type: userConstants.PROFILE_FAILURE, error } }	

}


function getData() {

    return dispatch => {
        dispatch(request(""));
    };

    function request(data) { return { type: userConstants.DATA_REQUEST, data }}

}