import React, { Component } from 'react';

class ProfileWidget extends Component {


	// pass the action in from the controller
	componentDidMount() {
		console.log("ProfileWidget: componentDidMount");
		this.props.loader()
	}

	render() {

    	console.log("ProfileWidget.render()");

    	const { user, profile } = this.props;

 		return (

 			<div>
	        	<p>Hi {user.firstName} </p>
	        	<p>You're logged in with skelereact!</p>

				{profile.loading && <em>Loading profile...</em>}
	        	{profile.alpha && 
	        		<div>
	            		<p>Alpha: {profile.alpha}</p>
	            		<p>Beta: {profile.beta}</p>
	            		<p>Time: {profile.time}</p>
	            	</div>
	        	}
	        </div>

		);
 	}
 }

export default ProfileWidget;