// data reducer

import { userConstants } from '../constants';

export function data(state = {}, action) {

  switch (action.type) {
    case userConstants.DATA_REQUEST:
      return {
        loading: true
      };    
    default:
      return state
  }
}