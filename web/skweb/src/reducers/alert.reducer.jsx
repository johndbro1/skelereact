import { alertConstants } from '../constants';

/*
	For the purposes of this demo, the alert action logic is minimal, just here to display a modest bit of
	information on the screen.
 */
export function alert(state = {}, action) {
  switch (action.type) {
    case alertConstants.SUCCESS:
      return {
        type: 'alert-success',
        message: action.message
      };
    case alertConstants.ERROR:
      return {
        type: 'alert-danger',
        message: action.message
      };
    case alertConstants.CLEAR:
      return {};
    default:
      return state
  }
}