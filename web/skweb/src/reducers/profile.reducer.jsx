// profile reducer

import { userConstants } from '../constants';

export function profile(state = {}, action) {

  switch (action.type) {
    case userConstants.PROFILE_SUCCESS:

      console.log("profileReduce: profile success: "+JSON.stringify(action.profile));


      // if we return the profile object as an object, we get two layers of "profile" in the state,
      // because the function name is alrady providing the key "profile"
      // by using ...action.profile, we're telling javascript to expand the object and return the
      // contents, rather than returning the object
      return {
        ...action.profile
      };
    case userConstants.PROFILE_REQUEST:
      return {
        loading: true
      };    
    case userConstants.PROFILE_FAILURE:

      console.log("profileReduce: profile failure: "+JSON.stringify(action.error));

      return {
      	error: action.error
      }
    default:
      return state
  }
}