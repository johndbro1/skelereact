import { authConstants } from '../constants';


/*
** this allows us to remember the user across page refreshes
*/
let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? { loggedIn: true, user } : {};


/*
** The standard state management
*/
export function authentication(state = initialState, action) {
  switch (action.type) {
    case authConstants.LOGIN_REQUEST:
      return {
        loggingIn: true,
        // user: action.user   // this is unnnecessary, since we don't have a user at this point.
      };
    case authConstants.LOGIN_SUCCESS:

      const user = action.user;

      // the ...user construct expands all the sub-elements and returns them as an anonymous
      // set of "things", not a user object with children.
      //
      // for example:   user: { name: "John", role: "Admin" } becomes just the stuff inside:  { name: "John", role: "Admin"}
      // so you don't have to drill down through user to get to the information
      //
      return {
        loggedIn: true,
        ...user
      };
    case authConstants.LOGIN_FAILURE:
      return {};
    case authConstants.LOGOUT:
      return {};
    default:
      return state
  }
}