import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
// import { userActions } from '../actions';
import { ProfileController, DataController } from '../controllers';

class HomePage extends Component {

	// // when this runs, we go over to the server and fetch data.  Typically, this runs
	// // when the component is initialized, but not when it re-renders.
 //    componentDidMount() {

 //    	console.log("HomePage.componentDidMount");

 //    	// we need to dispatch a call to fetch the user profile
 //        this.props.dispatch(userActions.getProfile());

 //        // this.props.loader()

 //    }


    /*
		fetch the user and the profile data from the properties - these will get
		updated when the reducers run and the state is mapped to properties.

		So at this point, we just have to display what we have.
     */
    render() {

    	console.log("HomePage.render()");

    	const { user } = this.props;

    	/*
    		a fun technique we're using here is the squiggly brace to provide
    		logical control of the HTML.  when profile.loading is true, we
    		show loading, but when it is false, we don't.  

    		When the profile is loaded, we can tell because profile.alpha now
    		has a value (in a real app, this would be profile.loaded or something 
    		like that)

    		Then we just have to display the content
    	 */

        return (

        	<div>
	        	<div className="row justify-content-end">
	        		<em>You are logged in as {user.firstName} {user.lastName}</em>
	        	</div>

	            <div className="row justify-content-center">

	            	<div className="col-md">
	            		<h2>First Column</h2>

	            		<ProfileController />

		            </div>

		            <div className="col-md">
			            <h2>Second Column</h2>
			            <DataController />
		            </div>

		            <div className="col-md">
		            	<h3>Third Column</h3>
		                <p>
		                    <Link to="/login">Logout</Link>
		                </p>
		            </div>

	            </div>
            </div>
        );
    }
}

function mapStateToProps(state) {

	console.log("HomePage: mapStateToProps: state is: "+JSON.stringify(state));

	/*
		You  may have noticed that these names "profile" and "authentication" happen to 
		also be the names of reducer functions, as found in profile.reducer.jsx and
		authentication.reducer.jsx.

		That's not a coincidence, the function names are what determine where profile
		and authentication information is stored inside the state object.

		another way to write this would be the more explicit:

		const profile = state.profile
		const authentication = state.authentication
	 */

	const { profile, authentication } = state;


	console.log("HomePage: mapStateToProps: profile is: "+JSON.stringify(profile));

	// user is already a property inside authentication
	// another way to write this would be:
	// const user = authentication.user
	//
	const { user } = authentication; 

	// this is now aligned perfect with the 2 properties in the render function above
	return {
		user		
	}

}

const connectedHomePage = connect(mapStateToProps)(HomePage);
export { connectedHomePage as HomePage };


/*

            		<h2>Home Page Version</h2>
	            	<p>Hi {user.firstName} </p>
	            	<p>You're logged in with skelereact!</p>

					{profile.loading && <em>Loading profile...</em>}
	            	{profile.alpha && 
	            		<div>
		            		<p>Alpha: {profile.alpha}</p>
		            		<p>Beta: {profile.beta}</p>
		            		<p>Time: {profile.time}</p>
		            	</div>
	            	}

*/