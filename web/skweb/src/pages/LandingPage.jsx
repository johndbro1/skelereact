import React from "react";
import { 
  Link
} from "react-router-dom";

/*
	The Landing page is super-simple - it doesn't really have to do anything other than display some
	content and provide a login link.

	We could make this more sophisticated, fetching data from the server &c, but that's not necessary
	in such a standard demo.
 */
export const LandingPage = () => { 

	return(

	<div className="row justify-content-center">
		<div className="col-md">
			<h3>Landing</h3>
			Welcome to skelereact.

		      <ul>
		        <li>
		          <Link to="/login">Login</Link>
		        </li>
		      </ul>
		</div>
	</div>
	);
}
