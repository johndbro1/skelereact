import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import { connect } from 'react-redux';

import { authActions, alertActions } from '../actions';

/*
	One of the biggest differences between the LoginPage component and the HomePage component is
	the minimal dependence on redux.  This is on purpose - it's a security hazard to carry around
	the password in the state, because it would be available to every component in the application
	(and if you used a third-party component, it might do something sneaky). 

	So this way, we keep the username and password hidden here, where other parts of the system can't
	get to them (at least in theory).
 */
class LoginPage extends Component {


    constructor(props) {

    	console.log("LoginPage constructor");

        super(props);

        // reset login status - sets various redux state values to null
        //
        this.props.dispatch(authActions.logout());
        this.props.dispatch(alertActions.clear());         

        // set our state as a starting point.
        this.state = {
            username: '',
            password: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    // this is called as data is entered, key by key (and in fact, the login page is re-rendered
    // on every change, so there's a lot of re-rendering)
    // 
    // we've lined up the state variables 'username' and 'password' with the text boxes (which have
    // the same names.  That's a bit of a hack, ideally we should probably use a ref so we don't have
    // specific dependencies on names.  
    //
    handleChange(e) {

        const { name, value } = e.target;

    	// console.log("LoginPage: handleChange: name: "+name+"; value: "+value);

        this.setState({ [name]: value });
    }

    handleSubmit(e) {

    	// The Event interface's preventDefault() method tells the user agent that if the event 
    	// does not get explicitly handled, its default action should not be taken as it normally would be
        e.preventDefault();

        // the user pressed submit, so we can set that to true in the state.
        this.setState({ submitted: true });
        const { username, password } = this.state;
        const { dispatch } = this.props;
        if (username && password) {

        	// this is one of two place we're using redux - we dispatch the login action
        	// of the auth.actions.jsx group.  We pass our browser history as the 
        	// third parameter, so that the login action can reroute the user to 
        	// the home page when the login is successrul.  Where do we get the browser
        	// history, and/or how did it end up in our properties, even though we don't
        	// set it above?  More on that at the end of this file.
        	//
        	//   aside - when you look at auth.actions login() method, note that
        	//           it returns a dispatch function, not an action object.
        	//           that's because it's depending on thunk to execute that 
        	//           function at a future time and place.
        	//
        	// I've already provided a detailed breakdown of the login function in 
        	// auth.actions.jsx , so go read that if you like.
        	//
            dispatch(authActions.login(username, password, this.props.history));
        }
    }

    render() {

    	console.log("LoginPage: render() ");

    	// this is the other thing we're getting from redux - the loggingIn value is set in the
    	// auth.action and propogated via the authentication.reducer so we can set it here.
    	// we don't have to use loggingIn - it would not be unreasonable to animate the 
    	// spinner locally, when the user hits submit (although we'd have to deal with edge cases)
    	// 
    	// In any case, loggingIn will change in value as the services are called, and that will
    	// control whether the spinner shows up and animates here, or not.
        const { loggingIn } = this.props;
        const { username, password, submitted } = this.state;
        return (
            <div className="row justify-content-center">
                <div className="col-md"></div>
                <div className="col-md">

                    <h2>Login</h2>
                    <form name="form" onSubmit={this.handleSubmit}>
                        <div className={'form-group' + (submitted && !username ? ' has-error' : '')}>
                            <label htmlFor="username">Username</label>
                            <input type="text" className="form-control" name="username" value={username} onChange={this.handleChange} />
                            {submitted && !username &&
                                <div className="help-block">Username is required</div>
                            }
                        </div>
                        <div className={'form-group' + (submitted && !password ? ' has-error' : '')}>
                            <label htmlFor="password">Password</label>
                            <input type="password" className="form-control" name="password" value={password} onChange={this.handleChange} />
                            {submitted && !password &&
                                <div className="help-block">Password is required</div>
                            }
                        </div>
                        <div className="form-group">
                            <button className="btn btn-primary">Login</button>
                            {loggingIn &&
                                <img alt="" src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                            }
                        </div>
                    </form>

                </div>
                <div className="col-md"></div>
            </div>
        );
    }
}


function mapStateToProps(state) {

	console.log("LoginPage - mapStateToProps - received state update: "+JSON.stringify(state));

	// the only information we care about is the loggingIn sub-element of state.authentication
	// and that is provided by the authentication.reducer when we get a login request
	//
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

/*
	I teased the browser history earlier, and this is the reveal - withRouter() is a method
	that "decorates" a component with new functionality.  In this case, specifically, it adds
	the react-router history subsystem to the LoginPage's properties.   

	That decoration is what allows us to pass the history down to the action when we call
	login, and that is what in turn allows the action to then tell the routing system that
	it should consider revealing the HomePage (if the user is successfully authenticated)
 */
const connectedWithRouterLoginPage = withRouter(connect(mapStateToProps)(LoginPage));
export { connectedWithRouterLoginPage as LoginPage }; 


/*

            <div className="col-md-6 col-md-offset-3">
                <h2>Login</h2>
                <form name="form" onSubmit={this.handleSubmit}>
                    <div className={'form-group' + (submitted && !username ? ' has-error' : '')}>
                        <label htmlFor="username">Username</label>
                        <input type="text" className="form-control" name="username" value={username} onChange={this.handleChange} />
                        {submitted && !username &&
                            <div className="help-block">Username is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !password ? ' has-error' : '')}>
                        <label htmlFor="password">Password</label>
                        <input type="password" className="form-control" name="password" value={password} onChange={this.handleChange} />
                        {submitted && !password &&
                            <div className="help-block">Password is required</div>
                        }
                    </div>
                    <div className="form-group">
                        <button className="btn btn-primary">Login</button>
                        {loggingIn &&
                            <img alt="" src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                        }
                    </div>
                </form>
            </div>

*/