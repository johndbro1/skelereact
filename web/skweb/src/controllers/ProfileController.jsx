import { connect } from 'react-redux';
import ProfileWidget from '../widgets/ProfileWidget';
import { userActions } from '../actions';



const mapStateToProps = state => {

	console.log("ProfileController: mapStateToProps: state is: "+JSON.stringify(state));

	/*
		You  may have noticed that these names "profile" and "authentication" happen to 
		also be the names of reducer functions, as found in profile.reducer.jsx and
		authentication.reducer.jsx.

		That's not a coincidence, the function names are what determine where profile
		and authentication information is stored inside the state object.

		another way to write this would be the more explicit:

		const profile = state.profile
		const authentication = state.authentication
	 */

	const { profile, authentication } = state;


	console.log("ProfileController: mapStateToProps: profile is: "+JSON.stringify(profile));

	// user is already a property inside authentication
	// another way to write this would be:
	// const user = authentication.user
	//
	const { user } = authentication; 


	// this is now aligned perfect with the 2 properties in the render function above
	return {
		user, 
		profile
	}

}

const mapDispatchToProps = (dispatch) => {
	console.log("ProfileController - creating loader for widget");
	return {
		loader: () => {
			dispatch(userActions.getProfile());
		}
	}
}



export const ProfileController = connect(mapStateToProps, mapDispatchToProps )(ProfileWidget);

// export default ProfileController;