import { connect } from 'react-redux';
import DataWidget from '../widgets/DataWidget';
import { userActions } from '../actions';



const mapStateToProps = state => {

	console.log("DataController: mapStateToProps: state is: "+JSON.stringify(state));

	const { authentication, data } = state;


	// user is already a property inside authentication
	// another way to write this would be:
	// const user = authentication.user
	//
	const { user } = authentication; 


	// this is now aligned perfect with the 2 properties in the render function above
	return {
		user, 
		data
	}

}

const mapDispatchToProps = (dispatch) => {
	console.log("DataController - creating loader for widget");
	return {
		loader: () => {
			dispatch(userActions.getData());
		}
	}
}


export const DataController = connect(mapStateToProps, mapDispatchToProps )(DataWidget);
