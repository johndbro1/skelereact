
import { authHeader } from '../helpers';


export const userService = {
    getProfile,
};


/*
 *  This function calls the server to fetch the profile for the user.
 *  The auth_header tells the server which user to provide the profile for, and it's
 *  expected to be impossible to fake.
 *
 *  
 */
function getProfile() {

    const profileUrl = "http://" + window.location.hostname + ":8080/api/profile";

	const auth_header = authHeader();

	console.log("userSevice.getProfile calling url: "+profileUrl+" with authHeader: "+JSON.stringify(auth_header));

    return fetch(profileUrl, { method: 'GET', headers: auth_header })
    	.then(response => {

    		/*
    			we have a response, if it is not OK, log the error and throw the error message, so it can 
    			be immediately caught by the error handler.
    		*/

		    if (!response.ok) { 

		    	console.warn("user.service.jsx - fetch response: bad response from server: "+JSON.stringify(response.status));

		        throw response.statusText;		    	
		    }

		    /*
		    	if there was no error, return the content to the next phase of the Promise chain.
		    */

		    return response.json()
   		
    	})
    	.catch( error => {

    		/*
    			there was an error fetching the data, so we are going to reject the Promise
    		*/
        	console.error("user.service.jsx - fetch error: "+error);
        	return Promise.reject("Server is unreachable");

    	}).then( json => {

    		console.log("userService.fetch response OK - json: "+JSON.stringify(json));

    		return json;
    	});
}



// function handleResponse(response) {

//     const json = response.json();

//     console.log("userService.handleResponse: OK response json: "+JSON.stringify(json));
//     return json;
// }