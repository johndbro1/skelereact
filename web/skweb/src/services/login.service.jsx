// import { authHeader } from '../helpers';  // we don't need this for the login service, we need it for the next service





export const loginService = {
    login,
    logout,
};


/*
	The login service attempts to reach the pre-defined API we have running on port 8080 of the same
	machine that is serving this React javascript.

	it POSTS the username and password (note that this is completely unsecure, so use https in a
	real environment.)

	And then it waits for a response.   If the response is bad, we throw an error, which will be 
	managed by the auth.action.jsx code.

	Otherwise, we pull the appropriate object(s) out of the response message, and return them, and
	the auth.action.jsx code will complete and start doing what it needs to do in the success case.
 */
function login(username, password) {

	console.log("login.service.jsx login() - start");

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password })
    };

    let loginUrl = "http://" + window.location.hostname + ":8080/api/login";

    console.log("login.service login() - About to fetch: "+loginUrl);


    return fetch(loginUrl, requestOptions)
        .then(http_response => {


        	// if the server didn't respond with a 200, we should log it, and then throw an error,
        	// which will immediately be caught, modified and rethrown.
            if (!http_response.ok) { 
            	console.warn("login.service.jsx - fetch - http_response: "+http_response.status+", "+http_response.statusText);
            	throw http_response;
            }

            // success!  log he success, and return the json data we received.
            const json = http_response.json();
            console.log("We received a valid http response: "+json)

            return json

        }).catch( error => {

        	/*
        		by putting the error handler here, we're ensuring it only handles errors from the http response,
        		not any data errors.  We presume the auth.actions.jsx already has handlers for data errors.
        	 */

        	console.warn("login.serrvice.jsx - error during fetch: "+error);

        	// rethrowing the error sends it up to the next error handler, which, in this case, is in
        	// auth.actions.jsx
        	const errMsg = {success: false, message: "Server is unreachable" };

        	// Pattern - never reject a promise in a then() clause, always reject promises in 
        	// catch clauses.  This helps reduce the chance that we end up catching our own
        	// error in an in appropriate place.
        	//
	        return Promise.reject(errMsg);

        }).then(login_data => {

        	console.log("fetch result: login_data: "+JSON.stringify(login_data));

            // login is successful if success is true, we have a user, and that user has a token.
            //
            if (!login_data.success || !login_data.user || !login_data.user.token) {

            	// something went wrong, log it.
            	console.warn("login.service.jsx - fetch result, bad login_data: "+JSON.stringify(login_data));

            	// this causes the alert to show 'invalid username or password'
            	throw login_data;

			}

			// confirmed success - good response and good data.   Save the data to local storage, 
			// and then return it.
        	console.log("login.serrvice.jsx - fetch result: valid login data: "+JSON.stringify(login_data));

            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('user', JSON.stringify(login_data.user));

       		return login_data;

        }).catch( login_failure => {

        	return Promise.reject(login_failure);
        });
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}

