What I've learned so far with React, Redux, Router, Thunk, etc



#React

1) Separate your display components (which I am calling widgets) from the logic that controls them.


#Redux

1) Redux is the javascript equivalent of a service bus - data gets put on the bus by actions, and reducers process the actions to change the state.
	and then react, which is basically event driven on state, will notice that state has changed and update all the
	components that care about the changed state.



#Thunk

Thunk is a way to more elegantly handle asynchronous events, although at this point, I don't really understand how it does that.


#Router

Router is a way to control what's displayed in your apps in the "full page" experience.

Pages are essentially collections of widgets, laid out in various ways.  Ideally, anything that you repeat on more than one page should be a widget, if possible.



#Overall

Basically, react is a way to get to an MVC system in the browser (or native) and your back-end is just services.   That's a good thing, because shipping all that html from the client to the server is wasteful and hard to maintain.   You'll still have business logic in the back end, but it will be about the data and the servers and the centralized stuff.   





#Application Flow

1) If you're not authenticated, you should be directed to the login page.  There, you will be presented with a form.  
2) As far as I can tell, normally, form data goes through redux so you can validate it and such, but for login data that's not a good idea.
3) User types username and password, and hits submit.
3.5) We'd like to display some sort of animation to show that the application is doing something.
4) Submit button onClick function will invoke logic that will reach out to the server and consume the login api, providing the username and password.
5) Back-end will validate (or not) and send data back to the client eventually
6) Client receives data, stops the animation, and looks at the data.   
6.1) On success, we update state that the App cares about?  Or we redirect back to the home page?.  Probably have to redirect to the home page if we're using router
6.2) On failure, we update state that the login page cares about.  This presents an error message on the login page, and allows them to try again
7) When we finally load the home page, we'll make a second api call to the back end to get the greeting message.



#Design implications

1) I need multiple reducers, one for authentication, one for the alerts.  Maybe one for the animation?  

2) I need to make sure the App cares about the authentication state, and that the Login Form (or some widget subcomponent) cares about the animation and the alerts.

#Questions

A) figure out how we can redirect or whatever from the login page so the home page gets a chance to be seen.
B) Need to have a better understanding of how to use the two mapping functions

