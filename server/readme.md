This is the server side of skelereact .  It will include a single landing page for web traffic and
a RESTful login api for client traffic.   

the concept is to provide all the API calls through this interface, and provide the possibility of a "classic" web interface via Spring MVC.   In my mind, this web interface would be exclusively used for administrative services that we would want to keep separate from the API.


