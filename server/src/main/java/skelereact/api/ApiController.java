package skelereact.api;

import org.springframework.web.bind.annotation.RestController;

import skelereact.model.JWTAuthResponse;
import skelereact.model.User;
import skelereact.model.UserAuthCredentials;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class ApiController {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/")
    public @ResponseBody String index() {
    	
    	log.info("index call");
    	
        return "{message: \"Welcome to skelereact Api\", index: true}";
    }

    
    @GetMapping("/blah")
    public @ResponseBody String blah() {
    	
    	log.info("blah call");
    	
        return "{message: \"Welcome to skelereact Api\", blah: true }";
    }
    
    
    @GetMapping("/profile")
    public @ResponseBody String profile() {
    	
    	String username = SecurityContextHolder.getContext().getAuthentication().getName();
    	
    	log.info("/profile username is: "+username);
    	
    	if ("user".equals(username)) {
    		
    		String data = "{ \"alpha\" : 1, \"beta\" : 2, \"time\" : \""+new Date()+"\" }";
    		log.info("/profile returning data for user: "+username+" : "+data);
    		
    		return data ;
    		
    	}
    	
    	return null;
    }
    
    
    @GetMapping("/login")
    public @ResponseBody String login() {
    	
    	log.info("login GET call");
    	
        return "{message: \"Welcome to skelereact Api\", login: true }";
    }
    

    @PostMapping("/login")
    public @ResponseBody JWTAuthResponse login( @RequestBody UserAuthCredentials userCreds ) {
    	
    	
    	log.info("login POST : "+userCreds);
    	
    	if (userCreds != null) {
    		
    		if ("user".equals(userCreds.getUsername())) {
    			
    			if ("password".equals(userCreds.getPassword())) {
    				
    				log.debug("login: Success!");
    				
    				User user = new User();
    				user.setFirstName("Joe");
    				user.setLastName("Somebody");
    				user.setUsername(userCreds.getUsername());
    				
    				return JWTAuthResponse.success(user);
    				
    			}
    		}

			log.debug("login: Failure!");
    		return JWTAuthResponse.failure();
    	}
    	
		log.debug("login: Error!");
    	return JWTAuthResponse.error("Unable to fetch user information");
    }
    
}

