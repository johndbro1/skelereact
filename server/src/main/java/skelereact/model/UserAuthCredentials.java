package skelereact.model;

public class UserAuthCredentials {
	
	private String username;
	private String password;
	
	public UserAuthCredentials(String user, String password) {
		this.username = user;
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "UserAuthCredentials [username=" + username + ", password=" + password + "]";
	}
	
	public UserAuthCredentials() {
		super();
	}
	
	
	
}
