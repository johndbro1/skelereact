package skelereact.model;

public interface JWTTokenizedUser {

	void setToken(String token);
	
	String getUsername();

//	String getToken();
}
