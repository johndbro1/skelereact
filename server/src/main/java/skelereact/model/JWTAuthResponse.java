package skelereact.model;

import java.util.Date;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import skelereact.Application;

public class JWTAuthResponse {
	
	
    final JWTTokenizedUser user;
	final boolean success;
	final String message;
	

	public JWTAuthResponse(JWTTokenizedUser user, boolean success, String message) {
		this.success = success;
		this.message = message;
		this.user    = user;
	}

	public JWTTokenizedUser getUser() {
		return user;
	}
	
	public boolean isSuccess() {
		return success;
	}

	public String getMessage() {
		return message;
	}

	public static JWTAuthResponse success(JWTTokenizedUser tokenizedUser) {
        
		final String token = Application.getJWTHelper().buildTokenWithSubject(tokenizedUser.getUsername());
		
        tokenizedUser.setToken(token);
        
        return new JWTAuthResponse(tokenizedUser, true, null);
	}

	public static JWTAuthResponse failure() {
		return new JWTAuthResponse(null, false, "Unknown username or password");
	}
	
	public static JWTAuthResponse error(String message) {
		return new JWTAuthResponse(null, false, message);
	}
	
	
}
