package skelereact;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import skelereact.filter.JWTAuthorizationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {

    	
    	/*
    	 * Figuring this out really sucked.  Lots of dead ends thinking I had the configuration wrong or that 
    	 * somehow I had to explicitly allow POST
    	 */
    	http.csrf().disable();
    	
    	
    	/*
    	 * Standard stuff related to remote access - anything in /public or /resources is open (CSS, HTML, Javascript)
    	 * 
    	 * Anything in the API is open, because we aren't using session management and we wouldn't want to carry that
    	 * stuff around in any case.
    	 */
    	http.authorizeRequests()
    		.antMatchers("/", "/public/**", "/resources/**", "/api/**").permitAll()
    		.antMatchers("/admin/**").hasRole("ADMIN")
    		.and()
    		.addFilter(new JWTAuthorizationFilter(authenticationManager()))
             // this disables session creation on Spring Security.  
    		 // NOTE: this may have to change if we have a web app for managing infrastructure, and a react app
    		 // for customers/ users.
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)    		
    		;
    	
    	

    }

    @Bean
    @Override
    public UserDetailsService userDetailsService() {
    	
    	/*
    	 * Just a stub for the purposes of demoing the basic model.
    	 */
        UserDetails user =
             User.withDefaultPasswordEncoder()
                .username("user")
                .password("password")
                .roles("USER")
                .build();

        return new InMemoryUserDetailsManager(user);
    }
}

