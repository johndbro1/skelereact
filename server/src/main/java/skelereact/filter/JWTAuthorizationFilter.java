package skelereact.filter;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import io.jsonwebtoken.Jwts;
import skelereact.Application;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	public JWTAuthorizationFilter(AuthenticationManager authManager) {
		super(authManager);
	}
	
	
    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {

    	log.debug("doFilterInternal - start");
    	
    	final String header = req.getHeader(Application.AUTH_HEADER_STRING);

    	log.debug("doFilterInternal - header: "+header);
    	
        if (header == null || !header.startsWith(Application.AUTH_TOKEN_PREFIX)) {
            chain.doFilter(req, res);
            return;
        }

        log.debug("doFilterInternal - fetching user/password");
        final UsernamePasswordAuthenticationToken authentication = getAuthentication(req);

        log.debug("doFilterInternal - saving auth object to context");
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        final String token = request.getHeader(Application.AUTH_HEADER_STRING);
        if (token != null) {
        	
            // parse the token.  We've included the username, which is very basic, but for
        	// the purposes of the demo, it should be sufficient.
            final String username = Application.getJWTHelper().getSubjectFromToken(token);

            if (username != null) {
            	log.info("getAuthentication: found username in token: "+username+", saving to security context.");
                return new UsernamePasswordAuthenticationToken(username, null, new ArrayList<>());
            }
            
            log.warn("getAuthentication: Unable to find username in JWT token using token prefix: "+Application.AUTH_TOKEN_PREFIX);
            return null;
        }
        
        log.warn("getAuthentication: Unable to find JWT token in headers using pattern: "+Application.AUTH_HEADER_STRING);
        return null;
    }
    
    
    
}
