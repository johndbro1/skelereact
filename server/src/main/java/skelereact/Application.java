package skelereact;

import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import skelereact.util.JWTHelper;

@SpringBootApplication
public class Application {

	
    public static final String AUTH_TOKEN_PREFIX = "Bearer ";
    public static final String AUTH_HEADER_STRING = "Authorization";
    public static final String AUTH_SECRET = "skelereact_secret_key";
    public static final long   AUTH_EXPIRATION_TIME = 864_000_000; // 10 days

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    
    public static synchronized JWTHelper getJWTHelper() {
    	    	
    	return new JWTHelper( AUTH_TOKEN_PREFIX, AUTH_HEADER_STRING, AUTH_SECRET, AUTH_EXPIRATION_TIME );
    }
    
    
    // @Bean
    // public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
    //     return args -> {

    //         System.out.println("Let's inspect the beans provided by Spring Boot:");

    //         String[] beanNames = ctx.getBeanDefinitionNames();
    //         Arrays.sort(beanNames);
    //         for (String beanName : beanNames) {
    //             System.out.println(beanName);
    //         }

    //     };
    // }

}

