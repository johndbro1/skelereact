package skelereact.util;

import java.util.Date;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import skelereact.Application;

public class JWTHelper {
	
	private final String tokenPrefix;
	private final String headerString;
	private final String secret;
	private final long duration;
	
	
	public JWTHelper(String tokenPrefix, String headerString, String secret, long duration) {
		super();
		this.tokenPrefix = tokenPrefix;
		this.headerString = headerString;
		this.secret = secret;
		this.duration = duration;
	}
	
	
	public String buildTokenWithSubject(String subject) {
		
        final String token = Jwts.builder()
                .setSubject(subject)
                .setExpiration(new Date(System.currentTimeMillis() + this.duration))
                .signWith(SignatureAlgorithm.HS512, this.secret.getBytes())
                .compact();
		
        return token;
	}
	
	
	public String getSubjectFromToken( String token ) {
				
		final String subject = Jwts.parser()
        .setSigningKey(this.secret.getBytes())
        .parseClaimsJws(token.replace(this.tokenPrefix, ""))
        .getBody()
        .getSubject();		
		
		return subject;
	}
	
	

}
