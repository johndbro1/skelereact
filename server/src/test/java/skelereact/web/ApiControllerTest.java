package skelereact.web;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import skelereact.Application;
import skelereact.model.JWTAuthResponse;
import skelereact.model.User;
import skelereact.model.UserAuthCredentials;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ApiControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void getHello() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.index").value(true))
                
                ;
                		
                		
//                		content().string(equalTo("{message: \"Welcome to skelereact Api\", index=\"true\"}")));
    }
    
    
    @Test
    public void getGoodToken() throws Exception {

    	UserAuthCredentials userAuth = new UserAuthCredentials("user", "password");
    	
    	ResultActions result = mvc.perform(MockMvcRequestBuilders.post("/api/login")
    			.content(asJsonString(userAuth))
    			.contentType(MediaType.APPLICATION_JSON)
    			.accept(MediaType.APPLICATION_JSON));
    	
    		result
    			.andExpect(status().isOk())
    			.andExpect(jsonPath("$.success").value(true))
    			.andExpect(jsonPath("$.user.firstName").value("Joe"))
    			;
    }
    
    
    @Test
    public void getBadToken() throws Exception {

    	UserAuthCredentials userAuth = new UserAuthCredentials("user", "user");
    	
    	mvc.perform(MockMvcRequestBuilders.post("/api/login")
    			.content(asJsonString(userAuth))
    			.contentType(MediaType.APPLICATION_JSON)
    			.accept(MediaType.APPLICATION_JSON))
    			.andExpect(status().isOk())
    			.andExpect(jsonPath("$.success").value(false));
    	
    }
    
    @Test
    public void testJWTAccess() throws Exception {
    	
    	User tokenizedUser = new User();
    	tokenizedUser.setUsername("user");
    	
    	JWTAuthResponse response = JWTAuthResponse.success(tokenizedUser);
    	
    	String bearerToken = Application.AUTH_TOKEN_PREFIX + " " + tokenizedUser.getToken();
    	
    	mvc.perform(MockMvcRequestBuilders.get("/api/profile").header("Authorization", bearerToken))
    		.andExpect(status().isOk())
    		.andExpect(jsonPath("$.alpha").value(1));
    	
    }
    
    
    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }  
    
}

